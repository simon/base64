#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>

struct code {
    const uint32_t *validmap;
    int encoded_atom_size, decoded_atom_size;
    int (*decode_atom)(const char *, unsigned char *);
    void (*encode_atom)(const unsigned char *, int, char *);
    char partial_fill_character; /* fills in any trailing partial atom */
};

static const uint32_t base64_validmap[8] = {
    /*    digits                                    */
    /*    vvv      uppercase  lowercase             */
    0, 0x23ff8800, 0x7fffffe, 0x7fffffe, 0, 0, 0, 0
    /*   =   /+                                     */
};

static int base64_decode_atom(const char *atom, unsigned char *out)
{
    int vals[4];
    int i, v, len;
    unsigned word;
    char c;
    
    for (i = 0; i < 4; i++) {
	c = atom[i];
	if (c >= 'A' && c <= 'Z')
	    v = c - 'A';
	else if (c >= 'a' && c <= 'z')
	    v = c - 'a' + 26;
	else if (c >= '0' && c <= '9')
	    v = c - '0' + 52;
	else if (c == '+')
	    v = 62;
	else if (c == '/')
	    v = 63;
	else if (c == '=')
	    v = -1;
	else
	    return 0;		       /* invalid atom */
	vals[i] = v;
    }

    if (vals[0] == -1 || vals[1] == -1)
	return 0;
    if (vals[2] == -1 && vals[3] != -1)
	return 0;

    if (vals[3] != -1)
	len = 3;
    else if (vals[2] != -1)
	len = 2;
    else
	len = 1;

    word = ((vals[0] << 18) |
	    (vals[1] << 12) |
	    ((vals[2] & 0x3F) << 6) |
	    (vals[3] & 0x3F));
    out[0] = (word >> 16) & 0xFF;
    if (len > 1)
	out[1] = (word >> 8) & 0xFF;
    if (len > 2)
	out[2] = word & 0xFF;
    return len;
}

static void base64_encode_atom(const unsigned char *data, int n, char *out)
{
    static const char base64_chars[] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    unsigned word;

    word = data[0] << 16;
    if (n > 1)
	word |= data[1] << 8;
    if (n > 2)
	word |= data[2];
    out[0] = base64_chars[(word >> 18) & 0x3F];
    out[1] = base64_chars[(word >> 12) & 0x3F];
    if (n > 1)
	out[2] = base64_chars[(word >> 6) & 0x3F];
    else
	out[2] = '=';
    if (n > 2)
	out[3] = base64_chars[word & 0x3F];
    else
	out[3] = '=';
}

static struct code base64_code = {
    base64_validmap, 4, 3, base64_decode_atom, base64_encode_atom, '='
};

static const uint32_t hex_validmap[8] = {
    /*    digits    uppercase   lowercase             */
    0, 0x03ff0000, 0x0000007e, 0x0000007e, 0, 0, 0, 0
};

static int hex_decode_atom(const char *atom, unsigned char *out) {
    int i, v, len;
    unsigned word;
    char c;

    word = 0;
    for (i = 0; i < 2; i++) {
	c = atom[i];
	if (c >= 'A' && c <= 'F')
	    v = c - 'A' + 10;
	else if (c >= 'a' && c <= 'f')
	    v = c - 'a' + 10;
	else if (c >= '0' && c <= '9')
	    v = c - '0';
	else
	    return 0;		       /* invalid atom */
	word = 16 * word + v;
    }

    out[0] = word;
    return 1;
}

static void hex_encode_atom_lower(const unsigned char *data, int n, char *out)
{
    static const char hex_chars[] = "0123456789abcdef";

    unsigned word = data[0];
    out[0] = hex_chars[(word >> 4) & 0xF];
    out[1] = hex_chars[word & 0xF];
}

static void hex_encode_atom_upper(const unsigned char *data, int n, char *out)
{
    static const char hex_chars[] = "0123456789ABCDEF";

    unsigned word = data[0];
    out[0] = hex_chars[(word >> 4) & 0xF];
    out[1] = hex_chars[word & 0xF];
}

static struct code hex_code_lowercase = {
    hex_validmap, 2, 1, hex_decode_atom, hex_encode_atom_lower, '0'
};
static struct code hex_code_uppercase = {
    hex_validmap, 2, 1, hex_decode_atom, hex_encode_atom_upper, '0'
};

const char usagemsg[] =
    "usage: base64 [-d] [filename]        decode from a file or from stdin\n"
    "   or: base64 -e [-cNNN] [filename]  encode from a file or from stdin\n"
    "where: -d     decode mode (default)\n"
    "       -e     encode mode\n"
    "       -cNNN  set number of chars per line for encoded output\n"
    "       --hex  encode/decode ASCII hex bytes instead of base64\n"
    "       --HEX  same as --hex, but output hex digits in upper case\n"
    " also: base64 --version              report version number\n"
    "       base64 --help                 display this help text\n"
    "       base64 --licence              display the (MIT) licence text\n"
    ;

void usage(void) {
    fputs(usagemsg, stdout);
}

const char licencemsg[] =
    "base64 is copyright 2001,2004,2016 Simon Tatham.\n"
    "\n"
    "Permission is hereby granted, free of charge, to any person\n"
    "obtaining a copy of this software and associated documentation files\n"
    "(the \"Software\"), to deal in the Software without restriction,\n"
    "including without limitation the rights to use, copy, modify, merge,\n"
    "publish, distribute, sublicense, and/or sell copies of the Software,\n"
    "and to permit persons to whom the Software is furnished to do so,\n"
    "subject to the following conditions:\n"
    "\n"
    "The above copyright notice and this permission notice shall be\n"
    "included in all copies or substantial portions of the Software.\n"
    "\n"
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\n"
    "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\n"
    "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\n"
    "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS\n"
    "BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN\n"
    "ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN\n"
    "CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n"
    "SOFTWARE.\n"
    ;

void licence(void) {
    fputs(licencemsg, stdout);
}

void version(void) {
    printf("base64, unknown version\n"); /*---buildsys-replace---*/
}

int main(int ac, char **av) {
    const struct code *code = &base64_code;
    int encoding = 0;
    int cpl = 64;
    FILE *fp;
    char *fname;
    char *eptr;

    fname = NULL;

    while (--ac) {
        char *v, *p = *++av;
        if (*p == '-') {
            while (*p) {
                char c = *++p;
                switch (c) {
                  case '-':
                    {
                        const char *opt = p+1;
                        p += strlen(p); /* consume this entire argument word */
                        if (!strcmp(opt, "hex")) {
                            code = &hex_code_lowercase;
                        } else if (!strcmp(opt, "HEX")) {
                            code = &hex_code_uppercase;
                        } else if (!strcmp(opt, "version")) {
                            version();
                            exit(0);
                        } else if (!strcmp(opt, "help")) {
                            usage();
                            exit(0);
                        } else if (!strcmp(opt, "licence") ||
                                   !strcmp(opt, "license")) {
                            licence();
                            exit(0);
                        } else {
                            fprintf(stderr, "base64: unknown long option"
                                    " '--%s'\n", opt);
                            exit(1);
                        }
                    }
                    break;
                  case 'v':
                  case 'V':
                    version();
                    exit(0);
                    break;
                  case 'h':
                  case 'H':
                    usage();
                    exit(0);
                    break;
                  case 'd':
                    encoding = 0;
                    break;
                  case 'e':
                    encoding = 1;
                    break;
                  case 'c':
		    /*
		     * Options requiring values.
		     */
		    v = p+1;
		    if (!*v && ac > 1) {
			--ac;
			v = *++av;
		    }
		    if (!*v) {
                        fprintf(stderr, "base64: option '-%c' expects"
                                " an argument\n", c);
			exit(1);
		    }
		    switch (c) {
		      case 'c':
			cpl = strtol(v, &eptr, 10);
			if (eptr && *eptr) {
			    fprintf(stderr, "base64: option -c expects"
				    " a numeric argument\n");
			    exit(1);
			}
			break;
		    }
		    p = "";
                    break;
                }
            }
        } else {
            if (!fname)
                fname = p;
            else {
                fprintf(stderr, "base64: expected only one filename\n");
                exit(0);
            }
        }
    }

    if (fname) {
        fp = fopen(fname, encoding ? "rb" : "r");
	if (!fp) {
	    fprintf(stderr, "base64: unable to open '%s': %s\n", fname,
		    strerror(errno));
	    exit(1);
	}
    } else
        fp = stdin;

    if (encoding) {
        unsigned char in[3];
        char out[4];
        int column;
        int i, n;

        assert(code->decoded_atom_size <= sizeof(in));
        assert(code->encoded_atom_size <= sizeof(out));

        column = 0;
        while (1) {
            n = fread(in, 1, code->decoded_atom_size, fp);
            if (n == 0) break;
            code->encode_atom(in, n, out);
            for (i = 0; i < code->encoded_atom_size; i++) {
                fputc(out[i], stdout);
                column++;
                if (cpl && column >= cpl) {
                    putchar('\n');
                    column = 0;
                }
            }
        }

        if (column > 0)
            putchar('\n');
    } else {
        char in[4];
        unsigned char out[3];
        int c, i, n, eof;

        assert(code->encoded_atom_size <= sizeof(in));
        assert(code->decoded_atom_size <= sizeof(out));

        eof = 0;
        do {
            for (i = 0; i < code->encoded_atom_size; i++) {
                do {
                    c = fgetc(fp);
                } while (c != EOF && !(code->validmap[c/32] &
                                       ((uint32_t)1<<(c%32))));
                if (c == EOF) {
                    eof = 1;
                    break;
                }
                in[i] = c;
            }
            if (i > 0) {
                if (i < code->encoded_atom_size) {
                    fprintf(stderr, "base64: warning: number of input encoded"
                            " characters was not a multiple of %d\n",
                            code->encoded_atom_size);
                    while (i < code->encoded_atom_size)
                        in[i++] = code->partial_fill_character;
                }
                n = code->decode_atom(in, out);
                fwrite(out, 1, n, stdout);
            }
        } while (!eof);
    }

    if (fname)
        fclose(fp);

    return 0;
}
